<?php

use App\Http\Controllers\ContrateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ServicosController;
use App\Models\Servico;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('en/services/{slug}', [ServicosController::class, 'index'])->name('servicos-en');
Route::get('es/servicios/{slug}', [ServicosController::class, 'index'])->name('servicos-es');
Route::get('servicos/{slug}', [ServicosController::class, 'index'])->name('servicos');

Route::post('servicos/mais-informacoes', [ServicosController::class, 'formPost'])->name('servicos.post');

Route::get('en/hire', [ContrateController::class, 'index'])->name('contrate-en');
Route::get('es/contratar', [ContrateController::class, 'index'])->name('contrate-es');
Route::get('contrate', [ContrateController::class, 'index'])->name('contrate');

Route::post('contrate', [ContrateController::class, 'formPost'])->name('contrate.post');

Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

// LANG
Route::get('lang/{lang}', function ($lang) {
    if (in_array($lang, ['pt', 'en', 'es'])) {
        Session::put('locale', $lang);
    }
    return back();
})->name('lang');

require __DIR__ . '/auth.php';
