import MobileToggle from "./MobileToggle";

MobileToggle();

$(document).ready(function () {
    // HOME - banners
    $(".banners")
        .on("cycle-initialized", function (e, o) {
            $(".banners").css("display", "block");
            console.log("xpto1");
        })
        .cycle({
            slides: ".banner",
            fx: "fade",
            speed: 800,
            timeout: 5000,
            pager: ".cycle-pager",
        });

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // LANG
    $("select[name=lang]").change(function selectRoute() {
        window.location = $(this)
            .find(`option[value=${this.value}]`)
            .data("route");
    });

    // Whatsapp floating
    $("#chatWhatsapp").floatingWhatsApp({
        phone: "+55" + whatsappNumero,
        popupMessage: "Seja muito bem-vindo(a) à EDC Serviços. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-edc-uni"><img src="' +
            imgMarcaWhatsapp +
            '" alt=""></div>' +
            '<div class="textos"><p class="titulo">EDC Serviços</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
