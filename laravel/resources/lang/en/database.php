<?php

return [
    'title'     => 'title_en',
    'capa'      => 'capa_en',
    'slug'      => 'slug_en',
    'titulo'    => 'titulo_en',
    'frase'     => 'frase_en',
    'texto'     => 'texto_en',
    'imagem'    => 'imagem_en',
    'vantagens_titulo' => 'vantagens_titulo_en',
    'vantagens_texto'  => 'vantagens_texto_en', 
    'atendimento' => 'atendimento_en',
    'endereco_pt1' => 'endereco_pt1_en',
    'endereco_pt2' => 'endereco_pt2_en',
    'itens'        => 'itens_en',
    'motivos1'     => 'motivos1_en',
    'motivos2'     => 'motivos2_en',
    'motivos3'     => 'motivos3_en',
    'frase_selo'   => 'frase_selo_en',
    'nome'         => 'nome_en',
    'servicos'     => 'servicos_en',
];
