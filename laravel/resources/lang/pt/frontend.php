<?php

return [
    '404' => 'Página não encontrada',

    'geral' => [
        'vagas'             => 'Vagas',
        'vagas-texto'       => 'Venha trabalhar com a gente. Confira nossas vagas',
        'area-do-candidato' => 'Área do Candidato',
        'area-do-consultor' => 'Área do Consultor',
        'contrate'          => 'CONTRATE',
        'termos'            => 'Termos de Uso',
        'politica'          => 'Política de Privacidade',
        'direitos'          => 'Todos os direitos reservados',
        'criacao'           => 'Criação de sites: ',
        'edc-group'         => 'É uma empresa do Grupo EDC',
    ],

    'home' => [
        'terc-temp'      => 'Terceiros e Temporários',
        'pq-contratar'   => 'POR QUE CONTRATAR A EDC SERVIÇOS?',
        'edc-menus'      => 'Histórico • Estrutura • Equipe • Notícias',
        'visite-site'    => 'visite o site da EDC Group »',
        'vagas'          => 'VAGAS',
        'link-vagas'     => 'Busque vagas e cadastre-se »',
        'frase-grupo'    => 'A EDC SERVIÇOS É UMA EMPRESA DA EDC GROUP • CONHEÇA AS EMPRESAS DO GRUPO:',
        'edc-servicos'   => 'EDC Serviços',
        'visite-website' => 'VISITE O WEBSITE',
        'edc-engenharia' => 'EDC Engenharia',
        'edc-uni'        => 'EDC UNI',
    ],

    'contrate-titulo'  => 'CONTRATE CONOSCO',
    'vantagens-titulo' => 'VANTAGENS DA CONTRATAÇÃO DE TEMPORÁRIOS',
    'mais-informacoes' => 'FALE COM UM ESPECIALISTA',

    'contato' => [
        'nome'        => 'nome',
        'telefone'    => 'telefone',
        'empresa'    => 'empresa',
        'cargo'    => 'cargo',
        'mensagem'    => 'mensagem',
        'msg-sucesso' => 'Mensagem enviada com sucesso!',
    ],

    'cookies1' => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa ',
    'cookies2' => 'Política de Privacidade',
    'cookies3' => ' e saiba mais.',
    'btn-cookies' => 'ACEITAR E FECHAR',
];
