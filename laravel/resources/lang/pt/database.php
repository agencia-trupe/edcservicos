<?php

return [
    'title'     => 'title_pt',
    'capa'      => 'capa_pt',
    'slug'      => 'slug_pt',
    'titulo'    => 'titulo_pt',
    'frase'     => 'frase_pt',
    'texto'     => 'texto_pt',
    'imagem'    => 'imagem_pt',
    'vantagens_titulo' => 'vantagens_titulo_pt',
    'vantagens_texto'  => 'vantagens_texto_pt', 
    'atendimento' => 'atendimento_pt',
    'endereco_pt1' => 'endereco_pt1_pt',
    'endereco_pt2' => 'endereco_pt2_pt',
    'itens'        => 'itens_pt',
    'motivos1'     => 'motivos1_pt',
    'motivos2'     => 'motivos2_pt',
    'motivos3'     => 'motivos3_pt',
    'frase_selo'   => 'frase_selo_pt',
    'nome'         => 'nome_pt',
    'servicos'     => 'servicos_pt',
];
