<?php

return [
    'title'     => 'title_es',
    'capa'      => 'capa_es',
    'slug'      => 'slug_es',
    'titulo'    => 'titulo_es',
    'frase'     => 'frase_es',
    'texto'     => 'texto_es',
    'imagem'    => 'imagem_es',
    'vantagens_titulo' => 'vantagens_titulo_es',
    'vantagens_texto'  => 'vantagens_texto_es', 
    'atendimento' => 'atendimento_es',
    'endereco_pt1' => 'endereco_pt1_es',
    'endereco_pt2' => 'endereco_pt2_es',
    'itens'        => 'itens_es',
    'motivos1'     => 'motivos1_es',
    'motivos2'     => 'motivos2_es',
    'motivos3'     => 'motivos3_es',
    'frase_selo'   => 'frase_selo_es',
    'nome'         => 'nome_es',
    'servicos'     => 'servicos_es',
];
