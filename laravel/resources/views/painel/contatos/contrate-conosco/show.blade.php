@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONTRATE CONOSCO</h2>
</legend>

<div class="mb-3 col-12">
    <label>Data</label>
    <div class="well">{{ $contatoContrate->created_at }}</div>
</div>

<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $contatoContrate->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $contatoContrate->email }}">
        <i class="bi bi-clipboard"></i>
        </button>
        {{ $contatoContrate->email }}
    </div>
</div>

@if($contatoContrate->telefone)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $contatoContrate->telefone }}</div>
</div>
@endif

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $contatoContrate->mensagem !!}</div>
</div>


<div class="d-flex align-items-center mt-4">
    <a href="{{ route('contrate-conosco.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop