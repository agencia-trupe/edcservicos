@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">SERVIÇOS - MAIS INFORMAÇÕES</h2>
</legend>

<div class="mb-3 col-12">
    <label>Data</label>
    <div class="well">{{ $contatoServicos->created_at }}</div>
</div>

<div class="mb-3 col-12">
    <label>Serviço</label>
    <div class="well">{{ $contatoServicos->servico }}</div>
</div>

<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $contatoServicos->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $contatoServicos->email }}">
        <i class="bi bi-clipboard"></i>
        </button>
        {{ $contatoServicos->email }}
    </div>
</div>

@if($contatoServicos->telefone)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $contatoServicos->telefone }}</div>
</div>
@endif

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $contatoServicos->mensagem !!}</div>
</div>


<div class="d-flex align-items-center mt-4">
    <a href="{{ route('mais-informacoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop