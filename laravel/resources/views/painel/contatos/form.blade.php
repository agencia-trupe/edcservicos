@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('whatsapp', 'Whatsapp') !!}
        {!! Form::text('whatsapp', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('atendimento_pt', 'Atendimento (PT)') !!}
        {!! Form::text('atendimento_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('atendimento_en', 'Atendimento (EN)') !!}
        {!! Form::text('atendimento_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('atendimento_es', 'Atendimento (ES)') !!}
        {!! Form::text('atendimento_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt1_pt', 'Endereço - Parte 1 (PT)') !!}
        {!! Form::text('endereco_pt1_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt1_en', 'Endereço - Parte 1 (EN)') !!}
        {!! Form::text('endereco_pt1_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt1_es', 'Endereço - Parte 1 (ES)') !!}
        {!! Form::text('endereco_pt1_es', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt2_pt', 'Endereço - Parte 2 (PT)') !!}
        {!! Form::text('endereco_pt2_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt2_en', 'Endereço - Parte 2 (EN)') !!}
        {!! Form::text('endereco_pt2_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt2_es', 'Endereço - Parte 2 (ES)') !!}
        {!! Form::text('endereco_pt2_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase Contato (PT)') !!}
        {!! Form::textarea('frase_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase Contato (EN)') !!}
        {!! Form::textarea('frase_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase Contato (ES)') !!}
        {!! Form::textarea('frase_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('facebook', 'Facebook (link)') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('linkedin', 'LinkedIn (link)') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('link_vagas', 'Vagas (link)') !!}
    {!! Form::text('link_vagas', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('contatos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>