@extends('painel.layout.template')

@section('content')

<h2>Bem Vindo(a),</h2>
<br>
<p>
    Pelo painel administrativo da
    <a href="http://www.trupe.net" target="_blank">Trupe</a>
    você pode controlar as principais funções e conteúdo do site.
</p>
<p>
    Em caso de dúvidas entrar em contato:
    <a href="mailto:contato@trupe.net">contato@trupe.net</a>
    <div><small>L v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})</small></div>
</p>

@endsection