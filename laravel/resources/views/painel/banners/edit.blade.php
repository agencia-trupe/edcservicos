@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>HOME - Banners |</small> Editar Banner</h2>
</legend>

{!! Form::model($banner, [
'route' => ['banners.update', $banner->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.banners.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection