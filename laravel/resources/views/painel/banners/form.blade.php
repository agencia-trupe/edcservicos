@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase (PT)') !!}
        {!! Form::textarea('frase_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase (EN)') !!}
        {!! Form::textarea('frase_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase (ES)') !!}
        {!! Form::textarea('frase_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('banners.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>