@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_pt', 'Imagem Terceiros/Temporários (PT)') !!}
        @if($home->imagem_pt)
        <img src="{{ url('assets/img/home/'.$home->imagem_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        {!! Form::file('imagem_pt', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_en', 'Imagem Terceiros/Temporários (EN)') !!}
        @if($home->imagem_en)
        <img src="{{ url('assets/img/home/'.$home->imagem_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        {!! Form::file('imagem_en', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_es', 'Imagem Terceiros/Temporários (ES)') !!}
        @if($home->imagem_es)
        <img src="{{ url('assets/img/home/'.$home->imagem_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        {!! Form::file('imagem_es', ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('itens_pt', 'Itens Terceiros/Temporários (PT)') !!}
        {!! Form::textarea('itens_pt', null, ['class' => 'form-control editor-padrao']) !!}
        <p class="obs">Separar itens por <strong>parágrafo</strong> (1 item por linha)</p>
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('itens_en', 'Itens Terceiros/Temporários (EN)') !!}
        {!! Form::textarea('itens_en', null, ['class' => 'form-control editor-padrao']) !!}
        <p class="obs">Separar itens por <strong>parágrafo</strong> (1 item por linha)</p>
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('itens_es', 'Itens Terceiros/Temporários (ES)') !!}
        {!! Form::textarea('itens_es', null, ['class' => 'form-control editor-padrao']) !!}
        <p class="obs">Separar itens por <strong>parágrafo</strong> (1 item por linha)</p>
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_pt', 'Por que contratar 1 (PT)') !!}
        {!! Form::textarea('motivos1_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_en', 'Por que contratar 1 (EN)') !!}
        {!! Form::textarea('motivos1_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_es', 'Por que contratar 1 (ES)') !!}
        {!! Form::textarea('motivos1_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_pt', 'Por que contratar 2 (PT)') !!}
        {!! Form::textarea('motivos2_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_en', 'Por que contratar 2 (EN)') !!}
        {!! Form::textarea('motivos2_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_es', 'Por que contratar 2 (ES)') !!}
        {!! Form::textarea('motivos2_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_pt', 'Por que contratar 3 (PT)') !!}
        {!! Form::textarea('motivos3_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_en', 'Por que contratar 3 (EN)') !!}
        {!! Form::textarea('motivos3_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_es', 'Por que contratar 3 (ES)') !!}
        {!! Form::textarea('motivos3_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_pt', 'Frase Selo 11 anos (PT)') !!}
        {!! Form::textarea('frase_selo_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_en', 'Frase Selo 11 anos (EN)') !!}
        {!! Form::textarea('frase_selo_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_es', 'Frase Selo 11 anos (ES)') !!}
        {!! Form::textarea('frase_selo_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('imagem_vagas', 'Imagem Vagas') !!}
    @if($home->imagem_vagas)
    <img src="{{ url('assets/img/home/'.$home->imagem_vagas) }}" style="display:block; margin-bottom: 10px; width:auto; max-width: 100%;">
    @endif
    {!! Form::file('imagem_vagas', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase Vagas (PT)') !!}
        {!! Form::textarea('frase_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase Vagas (EN)') !!}
        {!! Form::textarea('frase_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase Vagas (ES)') !!}
        {!! Form::textarea('frase_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('configuracoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>