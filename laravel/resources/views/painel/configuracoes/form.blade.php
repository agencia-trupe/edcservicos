@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('title_pt', 'Título (PT)') !!}
        {!! Form::text('title_pt', null, ['class' => 'form-control input-text']) !!}

        {{-- new --}}
        <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
            <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                Terceirização Title
            </div>
            <div> 
                {!! Form::text('terc_title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
            <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                Temporários Title
            </div>
            <div> 
                {!! Form::text('temp_title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
            <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                Contrate Title
            </div>
            <div> 
                {!! Form::text('contrate_title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('title_en', 'Título (EN)') !!}
        {!! Form::text('title_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('title_es', 'Título (ES)') !!}
        {!! Form::text('title_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>
<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('description_pt', 'Descrição (PT)') !!}
        {!! Form::text('description_pt', null, ['class' => 'form-control input-text']) !!}
    </div>

    {{-- new --}}
    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Terceirização Descrição
        </div>
        <div> 
            {!! Form::text('terc_description', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Temporários Descrição
        </div>
        <div> 
            {!! Form::text('temp_description', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Contrate Descrição
        </div>
        <div> 
            {!! Form::text('contrate_description', null, ['class' => 'form-control']) !!}
        </div>
    </div>


    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('description_en', 'Descrição (EN)') !!}
        {!! Form::text('description_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('description_es', 'Descrição (ES)') !!}
        {!! Form::text('description_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>
<div class="row mb-2">
    <div class="mb-3 col-6">
        {!! Form::label('keywords', 'Keywords') !!}
        {!! Form::text('keywords', null, ['class' => 'form-control input-text']) !!}
    </div>

    {{-- new --}}
    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Terceirização KeyWords
        </div>
        <div> 
            {!! Form::text('terc_keywords', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Temporários KeyWords
        </div>
        <div> 
            {!! Form::text('temp_keywords', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
        <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
            Contrate KeyWords
        </div>
        <div> 
            {!! Form::text('contrate_keywords', null, ['class' => 'form-control']) !!}
        </div>
    </div>



    <div class="mb-3 col-6">
        {!! Form::label('analytics_ua', 'Código Analytics (UA)') !!}
        {!! Form::text('analytics_ua', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6">
        {!! Form::label('analytics_g', 'Código Analytics (G)') !!}
        {!! Form::text('analytics_g', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6">
        {!! Form::label('codigo_gtm', 'Código Google Tag Manager (GTM)') !!}
        {!! Form::text('codigo_gtm', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6">
        {!! Form::label('pixel_facebook', 'Código Pixel do Facebook') !!}
        {!! Form::text('pixel_facebook', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6">
        {!! Form::label('tinify_key', 'Código Tinify (Compressão de Imagens)') !!}
        {!! Form::text('tinify_key', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('imagem_de_compartilhamento', 'Imagem de compartilhamento') !!}
    @if($configuracao->imagem_de_compartilhamento)
    <img src="{{ url('assets/img/'.$configuracao->imagem_de_compartilhamento) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_de_compartilhamento', ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('configuracoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>