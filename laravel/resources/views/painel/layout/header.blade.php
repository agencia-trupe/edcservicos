<nav class="navbar navbar-expand-lg navbar-dark bg-dark py-3">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand me-5" href="{{ url('/') }}">{{ config('app.name') }}</a>
            <button class="navbar-toggler float-end" id="mobile-toggle" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="collapseExample">
            @include('painel.layout.nav')

            <ul class="navbar-nav navbar-right" style="margin:0 0 0 auto;">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle ps-3 @if(Tools::routeIs(['usuarios*', 'configuracoes*', 'aceite-de-cookies*'])) active @endif" role="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-gear-fill"></i>
                        <i class="bi bi-caret-down-fill ms-1"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark dropdown-menu-end" aria-labelledby="navbarDarkDropdownMenuLink">
                        <li>
                            <a href="{{ route('aceite-de-cookies.index') }}" class="dropdown-item @if(Tools::routeIs('aceite-de-cookies*')) active @endif">Relatório de Cookies</a>
                        </li>
                        <li>
                            <a href="{{ route('configuracoes.index') }}" class="dropdown-item @if(Tools::routeIs('configuracoes*')) active @endif">Configurações</a>
                        </li>
                        <li>
                            <a href="{{ route('usuarios.index') }}" class="dropdown-item @if(Tools::routeIs('usuarios*')) active @endif">Usuários</a>
                        </li>
                        <li>
                            {!! Form::open(['route' => 'logout']) !!}
                            {!! Form::submit('Logout', ['class' => 'dropdown-item']) !!}
                            {!! Form::close() !!}
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>