<ul class="nav navbar-nav">

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['banners*', 'home*'])) active @endif" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Home <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('banners.index') }}" class="dropdown-item @if(Tools::routeIs('banners*')) active @endif">Banners</a>
            </li>
            <li>
                <a href="{{ route('home.index') }}" class="dropdown-item @if(Tools::routeIs('home*')) active @endif">Home</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('servicos.index') }}" class="nav-link px-3 @if(Tools::routeIs('servicos*')) active @endif">Serviços</a>
    </li>

    <li>
        <a href="{{ route('empresas-grupo.index') }}" class="nav-link px-3 @if(Tools::routeIs('empresas-grupo*')) active @endif">Empresas [Grupo EDC]</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['contatos*', 'contrate-conosco*', 'mais-informacoes*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @php $total = $contatosContrateNaoLidos + $contatosServicosNaoLidos; @endphp
            @if($total >= 1)
            <span class="label label-success ms-1">{{ $total }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('contatos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('contrate-conosco.index') }}" class="dropdown-item @if(Tools::routeIs('contrate-conosco*')) active @endif d-flex align-items-center">
                    Contrate conosco
                    @if($contatosContrateNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosContrateNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li>
                <a href="{{ route('mais-informacoes.index') }}" class="dropdown-item @if(Tools::routeIs('mais-informacoes*')) active @endif d-flex align-items-center">
                    Mais Informações
                    @if($contatosServicosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosServicosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>