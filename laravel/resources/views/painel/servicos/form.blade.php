@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('capa_pt', 'Capa do Serviço (PT)') !!}
        @if($submitText == 'Alterar')
        @if($servico->capa_pt)
        <img src="{{ url('assets/img/servicos/'.$servico->capa_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('capa_pt', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('capa_en', 'Capa do Serviço (EN)') !!}
        @if($submitText == 'Alterar')
        @if($servico->capa_en)
        <img src="{{ url('assets/img/servicos/'.$servico->capa_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('capa_en', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('capa_es', 'Capa do Serviço (ES)') !!}
        @if($submitText == 'Alterar')
        @if($servico->capa_es)
        <img src="{{ url('assets/img/servicos/'.$servico->capa_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('capa_es', ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_pt', 'Texto Serviço (PT)') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_en', 'Texto Serviço (EN)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_es', 'Texto Serviço (ES)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_pt', 'Imagem sobre Serviço (PT)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_pt)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_pt', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_en', 'Imagem sobre Serviço (EN)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_en)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_en', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_es', 'Imagem sobre Serviço (ES)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_es)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_es', ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_titulo_pt', 'Título Vantagens (PT)') !!}
        {!! Form::text('vantagens_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_titulo_en', 'Título Vantagens (EN)') !!}
        {!! Form::text('vantagens_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_titulo_es', 'Título Vantagens (ES)') !!}
        {!! Form::text('vantagens_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_texto_pt', 'Texto Vantagens (PT)') !!}
        {!! Form::textarea('vantagens_texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_texto_en', 'Texto Vantagens (EN)') !!}
        {!! Form::textarea('vantagens_texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagens_texto_es', 'Texto Vantagens (ES)') !!}
        {!! Form::textarea('vantagens_texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servicos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>