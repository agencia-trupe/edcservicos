@extends('frontend.layout.temporario_template')

@section('content')



<main class="servicos">


     <div class="center">
        <section class="servico">
            <img src="{{ asset('assets/img/servicos/'.$servico->{trans('database.capa')}) }}" class="img-capa" title="{{ $servico->{trans('database.titulo')} }}">
           
            <div class="t">{!! $servico->{trans('database.texto')} !!}</div>
           
            <img src="{{ asset('assets/img/servicos/'.$servico->{trans('database.imagem')}) }}" class="img-servico" title="{{ $servico->{trans('database.titulo')} }}">
            
            <p class="titulo-vantagens">{{ $servico->{trans('database.vantagens_titulo')} }}</p>
            
            <div class="t">{!! $servico->{trans('database.vantagens_texto')} !!}</div>
        </section>
    </div> 

     <section class="mais-infos">
        <div class="center">
            <p class="frase">

                {{ trans('frontend.mais-informacoes') }}

                <img src="{{ asset('assets/img/layout/seta-maisinfo.svg') }}" class="img-seta" title="Mais Informações">
            </p>
            <form action="{{ route('servicos.post') }}" method="POST" enctype="multipart/form-data" class="form-servicos">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="hidden" name="servico_id" value="{{ $servico->id }}">
                    <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">

                    <input type="text" name="empresa" placeholder="{{ trans('frontend.contato.empresa') }}" value="">
                    <input type="text" name="cargo" placeholder="{{ trans('frontend.contato.cargo') }}" value="">

                    <input type="hidden" name="origem" value='Temporários'>

                </div>
                <textarea name="mensagem" style='height:246px;' placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" style='height:246px;' class="btn-enviar">
                    <img src="{{ asset('assets/img/layout/icone-enviar.svg') }}" class="img-enviar" title="Enviar">
                </button>
            </form>
        </div>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
        </div>
        @endif
    </section> 

</main>


@endsection