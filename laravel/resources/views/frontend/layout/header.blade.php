<header>

    <section class="nav-topo">
        <div class="center">
            @if($contato->link_vagas)
            <a href="{{ Tools::parseLink($contato->link_vagas)}}" target="_blank" class="link-vagas">
                {{ trans('frontend.geral.vagas') }}
                <span>| {{ trans('frontend.geral.vagas-texto') }}</span>
            </a>
            @endif

            <article class="right">
                <a href="https://www.edcgroup.com.br/candidato" class="link-topo" target="_blank">
                    {{ trans('frontend.geral.area-do-candidato') }}
                </a>
                <a href="https://www.edcgroup.com.br/consultor" class="link-topo" target="_blank">
                    {{ trans('frontend.geral.area-do-consultor') }}
                </a>

                <select name="lang">
                    @foreach(['pt', 'en', 'es'] as $lang)
                    <option value="{{ $lang }}" @if(app()->currentLocale() === $lang) selected @endif data-route="{{ route('lang', $lang) }}">
                        {{ strtoupper($lang) }}
                    </option>
                    @endforeach
                </select>

                <div class="redes-sociais">
                    <a href="{{ $contato->facebook }}" target="_blank" class="facebook" title="Facebook"></a>
                    <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin" title="LinkedIn"></a>
                    <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram"></a>
                </div>
            </article>
        </div>
    </section>

    <button id="mobile-toggle" type="button" role="button" class="@if(Tools::routeIs('home')) home @endif">
        <span class="lines"></span>
    </button>
    <nav>
        @include('frontend.layout.nav')
    </nav>

</header>