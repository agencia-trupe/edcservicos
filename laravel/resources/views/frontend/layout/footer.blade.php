<footer>
    <div class="selos-footer">
        <div class="center">
            <div class="selo-iso">
                <img src="{{ asset('assets/img/layout/selo-qualidade-iso9001.svg') }}" alt="" class="img-selo">
                <img src="{{ asset('assets/img/layout/selo-qualidade-iso14001.svg') }}" alt="SLAC" class="img-selo">
                <div class="textos">
                    <p>Empresa com <strong>CERTIFICAÇÃO ISO 9001 e ISO 14001</strong> <br> 
                        Prestação de serviços de recrutamento e seleção de mão de obra terceirizada e temporária. <br>
                        A EDC Group é uma empresa com foco na Gestão da Qualidade e do Meio Ambiente com Certificação ISO 9001:2015 desde 2019 e ISO 14001:2015 desde 2022</p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="center">
        <article class="vagas-areas">
            @if($contato->link_vagas)
            <a href="{{ Tools::parseLink($contato->link_vagas)}}" target="_blank" class="link-vagas">
                {{ trans('frontend.geral.vagas') }}
            </a>
            @endif
            <a href="https://www.edcgroup.com.br/candidato" class="link-area" target="_blank">
                {{ trans('frontend.geral.area-do-candidato') }}
            </a>
            <a href="https://www.edcgroup.com.br/consultor" class="link-area" target="_blank">
                {{ trans('frontend.geral.area-do-consultor') }}
            </a>
        </article>

        <article class="marcas">
            <a href="{{ route('home') }}" class="link-home">
                <img src="{{ asset('assets/img/layout/marca-edc-servicos-branco.svg') }}" alt="{{ $config->{trans('database.title')} }}" class="img-logo">
            </a>
            <a href="https://www.edcgroup.com.br/" target="_blank" class="link-edc-group">
                <p class="frase">{{ trans('frontend.geral.edc-group') }}</p>
                <img src="{{ asset('assets/img/layout/marca-edc-group-branco.svg') }}" alt="EDC GROUP" class="img-edc-group">
            </a>
        </article>

        <article class="contatos">
            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook" title="Facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin" title="LinkedIn"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt=""></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram"><img src="{{ asset('assets/img/layout/ico-instagram-topo.svg') }}" alt=""></a>
            </div>
            <div class="informacoes">
                @php
                $telefone = "+55".str_replace(" ", "", $contato->telefone);
                $whatsapp = "+55".str_replace(" ", "", $contato->whatsapp);
                @endphp
                <a href="tel:+55{{ $contato->telefone }}" class="link-telefone">+55 {{ $contato->telefone }}</a>
                <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-telefone" target="_blank">+55 {{ $contato->whatsapp }}</a>
                <a href="mailto:{{ $contato->email }}" class="link-email">{{ $contato->email }}</a>
            </div>
            <p class="endereco">{{ $contato->{trans('database.endereco_pt1')} }}</p>
            <p class="endereco">{{ $contato->{trans('database.endereco_pt2')} }}</p>
        </article>

        <article class="nav-footer">
            <a href="{{ route('home') }}" class="link-footer @if(Tools::routeIs('home')) active @endif">
                <span>•</span>HOME
            </a>
            @foreach($servicos as $servico)
            @if(Lang::getLocale() == "en")
            <a href="{{ route('servicos-en', $servico->slug_en) }}" class="link-footer @if(url()->current() == route('servicos-en', $servico->slug_en)) active @endif">
                <span>•</span>{{ $servico->{trans('database.titulo')} }}
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('servicos-es', $servico->slug_es) }}" class="link-footer @if(url()->current() == route('servicos-es', $servico->slug_es)) active @endif">
                <span>•</span>{{ $servico->{trans('database.titulo')} }}
            </a>
            @else
            <a href="{{ route('servicos', $servico->slug_pt) }}" class="link-footer @if(url()->current() == route('servicos', $servico->slug_pt)) active @endif">
                <span>•</span>{{ $servico->{trans('database.titulo')} }}
            </a>
            @endif
            @endforeach

            @if(Lang::getLocale() == "en")
            <a href="{{ route('contrate-en') }}" class="link-footer @if(Tools::routeIs('contrate-en')) active @endif">
                <span>•</span>{{ trans('frontend.geral.contrate') }}
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('contrate-es') }}" class="link-footer @if(Tools::routeIs('contrate-es')) active @endif">
                <span>•</span>{{ trans('frontend.geral.contrate') }}
            </a>
            @else
            <a href="{{ route('contrate') }}" class="link-footer @if(Tools::routeIs('contrate')) active @endif">
                <span>•</span>{{ trans('frontend.geral.contrate') }}
            </a>
            @endif
            <a href="https://www.edcgroup.com.br/termos-de-uso" class="link-termos" target="_blank">
                » {{ trans('frontend.geral.termos') }}
            </a>

            <a href="https://www.edcgroup.com.br/politica-de-privacidade" class="link-politica" target="_blank">
                » {{ trans('frontend.geral.politica') }}
            </a>
        </article>
    </div>
    <article class="copyright">
        <p class="direitos"> © {{ date('Y') }} {{ config('app.name') }} - {{ trans('frontend.geral.direitos') }}</p>
        <span>|</span>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">{{ trans('frontend.geral.criacao') }}</a>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>

        <a href="#" class="link-trupe" style='padding:0px 3px 0px 3px;'>
            L v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})   
        </a>
    </article>
</footer>