<div class="center">
    <div class="nav-header">
        @foreach($servicos as $servico)
       
      {{--   @if(Lang::getLocale() == "en")
        <a href="{{ route('servicos-en', $servico->slug_en) }}" class="link-servico @if(url()->current() == route('servicos-en', $servico->slug_en)) active @endif">
            {{ $servico->{trans('database.titulo')} }}
        </a>
        @elseif(Lang::getLocale() == "es")

        <a href="{{ route('servicos-es', $servico->slug_es) }}" class="link-servico @if(url()->current() == route('servicos-es', $servico->slug_es)) active @endif">
            {{ $servico->{trans('database.titulo')} }}
        </a>
        @else
        <a href="{{ route('servicos', $servico->slug_pt) }}" class="link-servico @if(url()->current() == route('servicos', $servico->slug_pt)) active @endif">
            {{ $servico->{trans('database.titulo')} }}
        </a> 
      
        @endif --}}

        @endforeach

        <a href="/servico/terceirizacao" class="link-servico">
            TERCEIRIZAÇÃO
        </a>
        <a href="/servico/temporario" class="link-servico">
            TEMPORÁRIOS
        </a>

        @if(Lang::getLocale() == "en")
        <a href="{{ route('contrate-en') }}" class="link-contrate @if(Tools::routeIs('contrate-en')) active @endif">
            {{ trans('frontend.geral.contrate') }}
        </a>
        @elseif(Lang::getLocale() == "es")
        <a href="{{ route('contrate-es') }}" class="link-contrate @if(Tools::routeIs('contrate-es')) active @endif">
            {{ trans('frontend.geral.contrate') }}
        </a>
        @else
        <a href="{{ route('contrate') }}" class="link-contrate @if(Tools::routeIs('contrate')) active @endif">
            {{ trans('frontend.geral.contrate') }}
        </a>
        @endif

        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-edc-servicos.svg') }}" alt="{{ $config->{trans('database.title')} }}" class="img-logo">
        </a>
    </div>
</div>