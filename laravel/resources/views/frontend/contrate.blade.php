@extends('frontend.layout.contrate_template')

@section('content')

<main class="contrate">

    <div class="center">
        <section class="informacoes">
            <h1 class="titulo-pagina">
                {{ trans('frontend.contrate-titulo') }}
                <img src="{{ asset('assets/img/layout/icone-vagas.svg') }}" class="img-seta" title="Vagas">
            </h1>
            <p class="frase">{{ $contato->{trans('database.frase')} }}</p>

            <img src="{{ asset('assets/img/layout/img-contrate.png') }}" class="img-contrate" title="Contrate Conosco">

            <form action="{{ route('contrate.post') }}" method="POST" enctype="multipart/form-data" class="form-contrate">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">
                
                <input type="text" name="empresa" placeholder="empresa" required>
                <input type="text" name="cargo" placeholder="cargo" required>

                <input type="hidden" name='origem' value='Contrato'>
                
                <div style='background:#fff;width:100%;margin:0px 0px 5px 0px;'>
                    <select name="interesse" class='contato-interesse'>
                        <option value="#">interesse (Selecione)</option>
                        <option value="Terceirização">Terceirização</option>
                        <option value="Temporários">Temporários</option>
                        <option value="Outros">Outros</option>
                    </select>
                </div>
              
                    

                
                <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">
                    <img src="{{ asset('assets/img/layout/icone-enviar.svg') }}" class="img-enviar" title="Enviar">
                </button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
                </div>
                @endif

                <a href="https://www.edcgroup.com.br/candidato" target="_blank">
                    <div class="work_for_us">
                        <h1>TRABALHE CONOSCO</h1>
                        <P>Para consultar vagas e enviar seu curriculo acesse a <strong>ÁREA DO CANDIDATO EDC</strong></P>
                    </div>
                </a>
            </form>

            

            @php
            $telefone = "+55".str_replace(" ", "", $contato->telefone);
            $whatsapp = "+55".str_replace(" ", "", $contato->whatsapp);
            @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-telefone" target="_blank">+55 {{ $contato->whatsapp }}</a>
            <a href="tel:+55{{ $contato->telefone }}" class="link-telefone">+55 {{ $contato->telefone }}</a>
            <p class="atendimento">{{ $contato->{trans('database.atendimento')} }}</p>

            <div class="edc-group">
                <img src="{{ asset('assets/img/layout/marca-edc-group.svg') }}" alt="EDC GROUP" class="img-edc-group">
                <div class="endereco-completo">
                    <p class="endereco">{{ $contato->{trans('database.endereco_pt1')} }}</p>
                    <p class="endereco">{{ $contato->{trans('database.endereco_pt2')} }}</p>
                </div>
            </div>
        </section>
    </div>

    <section class="mapa">
        {!! $contato->google_maps !!}
    </section>


</main>

@endsection