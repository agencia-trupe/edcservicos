@extends('frontend.layout.template')

@section('content')

<main class="home">

    <section class="banners" style="display: none;">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <div class="overlay-textos">
                <p class="titulo">{{ $banner->{trans('database.titulo')} }}</p>
                <hr class="linha-banner">
                <p class="frase">{{ $banner->{trans('database.frase')} }}</p>
            </div>
        </div>
        @endforeach
        <div class=cycle-pager></div>
    </section>

    <section class="terceiros-e-temporarios">
        <div class="center">
            <img src="{{ asset('assets/img/home/'.$home->{trans('database.imagem')}) }}" class="img-terceiros" title="{{ trans('frontend.home.terc-temp') }}">
            <div class="itens-home">
                @php
                $itens = array_filter(explode('<p>', str_replace("</p>", "", $home->{trans('database.itens')})));
                @endphp
                @foreach($itens as $item)
                <p class="item"><span>»</span> {{ $item }}</p>
                @endforeach
            </div>
        </div>
    </section>

    <section class="contratar">
        <div class="center">
            <h2 class="titulo-verde">{{ trans('frontend.home.pq-contratar') }}</h2>
            <div class="motivos">
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-sevicos1.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos1')} }}</p>
                </article>
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-sevicos2.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos2')} }}</p>
                </article>
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-sevicos3.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos3')} }}</p>
                </article>
            </div>
            <div class="informacoes">


                @if(app()->getLocale() === 'pt')
                <img src="{{ asset('assets/img/layout/selo-12anos-pt.svg') }}" class="img-selo" title="12 anos">
                @endif
                
                @if(app()->getLocale() === 'en')
                <img src="{{ asset('assets/img/layout/selo-12anos-eng.svg') }}" class="img-selo" title="12 years">
                @endif

                @if(app()->getLocale() === 'es')
                <img src="{{ asset('assets/img/layout/selo-12anos-esp.svg') }}" class="img-selo" title="12 anõs">
                @endif

               

                <p class="frase-selo">{{ $home->{trans('database.frase_selo')} }}</p>

                <img src="{{ asset('assets/img/layout/marca-edc-group-slogan.svg') }}" class="img-edc-slogan" title="EDC Group">
                <div class="sobre-edc">
                    <p class="itens-edc">{{ trans('frontend.home.edc-menus') }}</p>
                    <div>
                        <a href="https://www.edcgroup.com.br/" class="link-edc" alt="Site EDC Group">{{ trans('frontend.home.visite-site') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="vagas">
        <div class="center">
            <img src="{{ asset('assets/img/home/'.$home->imagem_vagas) }}" class="img-terceiros" title="{{ trans('frontend.home.terc-temp') }}">
            <aside class="sobre-vagas">
                <h1 class="titulo-vagas">
                    {{ trans('frontend.home.vagas') }}
                    <img src="{{ asset('assets/img/layout/icone-vagas.svg') }}" class="img-seta" title="Vagas">
                </h1>
                <p class="frase-vagas">{{ $home->{trans('database.frase')} }}</p>
                <div>
                    <a href="{{ $contato->link_vagas }}" class="link-vagas" alt="">{{ trans('frontend.home.link-vagas') }}</a>
                </div>
            </aside>
        </div>
    </section>

    <section class="grupo-edc">
        <div class="center">
            <p class="frase-grupo">{{ trans('frontend.home.frase-grupo') }}</p>
            <article class="grupo">
                @foreach($empresas as $empresa)

                <a href="{{ $empresa->link }}" target="_blank" class="@if($empresa->id == 1) edc-servicos @elseif($empresa->id == 2) edc-engenharia @else edc-uni @endif" style="@if($empresa->id == 3) background-color:#65bba4 @endif">
                    <img src="{{ asset('assets/img/empresas/'.$empresa->logo) }}" class="img-logo" title="{{ $empresa->{trans('database.nome')} }}">
                    @if($empresa->nome_pt == 'EDC Smart')
                    <div class="servicos">
                        {!! $empresa->{trans('database.servicos')} !!}
                        <hr class="linha-divisao l1">
                    </div>
                    @else
                    <div class="servicos">
                        {!! $empresa->{trans('database.servicos')} !!}
                        <hr class="linha-divisao l1">
                        <hr class="linha-divisao l2">
                    </div>
                    @endif
                    <p class="@if($empresa->id == 1) link-servicos @elseif($empresa->id == 2) link-engenharia @else link-uni @endif" style="@if($empresa->id == 3) background-color:#65bba4 @endif">
                        {{ trans('frontend.home.visite-website') }}
                    </p>
                </a>
                
                @endforeach
            </article>
        </div>
    </section>

</main>

@endsection