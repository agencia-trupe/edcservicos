<!DOCTYPE html>
<html>

<head>
    <title>[SERVIÇOS - MAIS INFORMAÇÕES] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style="font-family:Verdana;color:#000;margin:0 0 20px 0;">
        <span style='font-size:18px'>Serviço:</span>
        <span style='font-size:16px;'>{{ $contato['servico_titulo'] }}</span>
    </p>
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Nome:</span>
        <span style='font-size:14px;'>{{ $contato['nome'] }}</span>
    </p>
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>E-mail:</span>
        <span style='font-size:14px;'>{{ $contato['email'] }}</span>
    </p>

    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Empresa:</span>
        <span style='font-size:14px;'>{{ $contato['empresa'] }}</span>
    </p>

    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Cargo:</span>
        <span style='font-size:14px;'>{{ $contato['cargo'] }}</span>
    </p>

    @if($contato['telefone'])
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Telefone:</span>
        <span style='font-size:14px;'>{{ $contato['telefone'] }}</span>
    </p>
    @endif
    <p style="font-family:Verdana;color:#000;margin:0 0 10px 0;">
        <span style='font-size:16px'>Mensagem:</span>
        <span style='font-size:14px;'>{{ $contato['mensagem'] }}</span>
    </p>
</body>

</html>