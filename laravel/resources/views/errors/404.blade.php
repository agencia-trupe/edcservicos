@extends('frontend.layout.template')

@section('content')

<div class="content">
    <div class="not-found">
        <h1>{{ trans('frontend.404') }}</h1>
    </div>
</div>

@endsection