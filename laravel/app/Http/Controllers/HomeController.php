<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\EmpresaGrupo;
use App\Models\Home;
use Illuminate\Http\Request;

use App\Models\ContrateConosco;

class HomeController extends Controller
{



   

    public function index()
    {

     

        $banners = Banner::ordenados()->get();
        $home = Home::first();
        $empresas = EmpresaGrupo::ordenados()->get();

        return view('frontend.home', compact('banners', 'home', 'empresas'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
