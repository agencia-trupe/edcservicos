<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresasGrupoRequest;
use App\Models\EmpresaGrupo;
use Illuminate\Http\Request;

class EmpresasGrupoController extends Controller
{
    public function index()
    {
        $empresas = EmpresaGrupo::ordenados()->get();

        return view('painel.empresas-grupo.index', compact('empresas'));
    }

    public function edit(EmpresaGrupo $empresas_grupo)
    {
        $empresa = $empresas_grupo;

        return view('painel.empresas-grupo.edit', compact('empresa'));
    }

    public function update(EmpresasGrupoRequest $request, EmpresaGrupo $empresas_grupo)
    {
        try {
            $input = $request->all();

            if (isset($input['logo'])) $input['logo'] = EmpresaGrupo::upload_logo();

            $empresas_grupo->update($input);

            return redirect()->route('empresas-grupo.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
