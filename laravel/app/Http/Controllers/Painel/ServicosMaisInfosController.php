<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\ServicoMaisInfo;
use Illuminate\Http\Request;

class ServicosMaisInfosController extends Controller
{
    public function index()
    {
        $contatos = ServicoMaisInfo::join('servicos', 'servicos.id', '=', 'servicos_mais_infos.servico_id')
            ->select('servicos.titulo_pt as servico', 'servicos_mais_infos.*')
            ->orderBy('servicos_mais_infos.created_at', 'DESC')->get();

        return view('painel.contatos.mais-informacoes.index', compact('contatos'));
    }

    public function show($contato)
    {
        $contatoServicos = ServicoMaisInfo::join('servicos', 'servicos.id', '=', 'servicos_mais_infos.servico_id')
            ->select('servicos.titulo_pt as servico', 'servicos_mais_infos.*')
            ->where('servicos_mais_infos.id', $contato)
            ->first();
        $contatoServicos->update(['lido' => 1]);

        return view('painel.contatos.mais-informacoes.show', compact('contatoServicos'));
    }

    public function destroy($contato)
    {
        try {
            $contatoServicos = ServicoMaisInfo::where('id', $contato)->first();
            $contatoServicos->delete();

            return redirect()->route('mais-informacoes.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($contato, Request $request)
    {
        try {
            $contatoServicos = ServicoMaisInfo::where('id', $contato)->first();
            $contatoServicos->update([
                'lido' => !$contatoServicos->lido
            ]);

            return redirect()->route('mais-informacoes.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
