<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosRequest;
use App\Models\Contato;
use Illuminate\Http\Request;

class ContatosController extends Controller
{


    public function index()
    {
        $registro = Contato::first();

        return view('painel.contatos.edit', compact('registro'));
    }

    public function update(ContatosRequest $request, $registro)
    {
        try {
            $registro = Contato::where('id', $registro)->first();

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('contatos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
