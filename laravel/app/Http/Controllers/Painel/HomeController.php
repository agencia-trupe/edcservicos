<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeRequest;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();

        return view('painel.home.edit', compact('home'));
    }

    public function update(HomeRequest $request, Home $home)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_pt'])) $input['imagem_pt'] = Home::upload_imagem_pt();
            if (isset($input['imagem_en'])) $input['imagem_en'] = Home::upload_imagem_en();
            if (isset($input['imagem_es'])) $input['imagem_es'] = Home::upload_imagem_es();
            if (isset($input['imagem_vagas'])) $input['imagem_vagas'] = Home::upload_imagem_vagas();

            $home->update($input);

            return redirect()->route('home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
