<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicosMaisInfosRequest;
use App\Models\Contato;
use App\Models\Servico;
use App\Models\ServicoMaisInfo;
use App\Notifications\ServicosMaisInfosNotification;
use Illuminate\Support\Facades\Notification;

class ServicosController extends Controller
{

    public function index($slug)
    {
        $servico = Servico::where('slug_pt', $slug)->first();
        if ($servico == null) {
            $servico = Servico::where('slug_en', $slug)->first();
            if ($servico == null) {
                $servico = Servico::where('slug_es', $slug)->first();
            }
        }

        return view('frontend.servicos', compact('servico'));
    }



    public function servterc()
    {
        //dd("x");
        $servico = Servico::find(1);


        return view('frontend.terceirizacao', ['servico' => $servico]);
    }

    public function servtemp()
    {
        //dd("x");
        $servico = Servico::find(2);


        return view('frontend.temporario', ['servico' => $servico]);
    }


    public function formPost(ServicosMaisInfosRequest $request, ServicoMaisInfo $maisInfosServico)
    {
        try {
            $data = $request->all();

            $maisInfosServico->create($data);

            $servico = Servico::where('id', $data['servico_id'])->first();

            $data['servico_titulo'] = $servico->titulo_pt;

            Notification::send(
                Contato::first(),
                new ServicosMaisInfosNotification($data)
            );

             session()->flash('success','Contato Enviado com Sucesso!'); 

            return redirect()->back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }
}
