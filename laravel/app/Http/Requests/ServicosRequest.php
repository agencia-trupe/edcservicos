<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa_pt'      => 'required|image',
            'capa_en'      => 'image',
            'capa_es'      => 'image',
            'titulo_pt'    => 'required',
            'titulo_en'    => '',
            'titulo_es'    => '',
            'texto_pt'     => 'required',
            'texto_en'     => '',
            'texto_es'     => '',
            'texto_es'     => '',
            'imagem_pt'    => 'required|image',
            'imagem_en'    => 'image',
            'imagem_es'    => 'image',
            'vantagens_titulo_pt' => 'required',
            'vantagens_titulo_en' => '',
            'vantagens_titulo_es' => '',
            'vantagens_texto_pt'  => 'required',
            'vantagens_texto_en'  => '',
            'vantagens_texto_es'  => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa_pt'] = 'image';
            $rules['capa_en'] = 'image';
            $rules['capa_es'] = 'image';
            $rules['imagem_pt'] = 'image';
            $rules['imagem_en'] = 'image';
            $rules['imagem_es'] = 'image';
        }

        return $rules;
    }
}
