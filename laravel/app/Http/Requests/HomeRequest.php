<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'imagem_pt'   => 'required|image',
            'imagem_en'   => 'image',
            'imagem_es'   => 'image',
            'itens_pt'    => 'required',
            'itens_en'    => '',
            'itens_es'    => '',
            'motivos1_pt' => 'required',
            'motivos1_en' => '',
            'motivos1_es' => '',
            'motivos2_pt' => 'required',
            'motivos2_en' => '',
            'motivos2_es' => '',
            'motivos3_pt' => 'required',
            'motivos3_en' => '',
            'motivos3_es' => '',
            'frase_selo_pt' => 'required',
            'frase_selo_en' => '',
            'frase_selo_es' => '',
            'imagem_vagas'  => 'required|image',
            'frase_pt'    => 'required',
            'frase_en'    => '',
            'frase_es'    => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_pt'] = 'image';
            $rules['imagem_en'] = 'image';
            $rules['imagem_es'] = 'image';
            $rules['imagem_vagas'] = 'image';
        }

        return $rules;
    }
}
