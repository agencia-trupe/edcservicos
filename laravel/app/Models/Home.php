<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_pt()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_pt', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_pt', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_en()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_en', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_en', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_es()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_es', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_es', [
                'width'  => 1200,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_vagas()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_vagas', [
                'width'  => 600,
                'height' => 280,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_vagas', [
                'width'  => 600,
                'height' => 280,
                'path'   => 'assets/img/home/'
            ]);
        }
    }
}
