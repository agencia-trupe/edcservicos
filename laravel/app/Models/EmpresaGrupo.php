<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpresaGrupo extends Model
{
    use HasFactory;

    protected $table = 'empresas_grupo';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_logo()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('logo', [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/empresas/'
            ]);
        } else {
            return CropImage::make('logo', [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/empresas/'
            ]);
        }
    }
}
