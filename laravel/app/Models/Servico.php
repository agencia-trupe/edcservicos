<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa_pt()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa_pt', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('capa_pt', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_capa_en()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa_en', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('capa_en', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_capa_es()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa_es', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('capa_es', [
                'width'  => 650,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_imagem_pt()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_pt', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_pt', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_imagem_en()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_en', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_en', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_imagem_es()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_es', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_es', [
                'width'  => 850,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }
}
