<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('imagem_pt');
            $table->string('imagem_en')->nullable();
            $table->string('imagem_es')->nullable();
            $table->text('itens_pt');
            $table->text('itens_en')->nullable();
            $table->text('itens_es')->nullable();
            $table->string('motivos1_pt');
            $table->string('motivos1_en')->nullable();
            $table->string('motivos1_es')->nullable();
            $table->string('motivos2_pt');
            $table->string('motivos2_en')->nullable();
            $table->string('motivos2_es')->nullable();
            $table->string('motivos3_pt');
            $table->string('motivos3_en')->nullable();
            $table->string('motivos3_es')->nullable();
            $table->string('frase_selo_pt');
            $table->string('frase_selo_en')->nullable();
            $table->string('frase_selo_es')->nullable();
            $table->string('imagem_vagas');
            $table->string('frase_pt');
            $table->string('frase_en')->nullable();
            $table->string('frase_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('home');
    }
}
