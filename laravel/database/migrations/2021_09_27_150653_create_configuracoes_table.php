<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfiguracoesTable extends Migration
{
    public function up()
    {
        Schema::create('configuracoes', function (Blueprint $table) {
            $table->id();
            $table->string('title_pt');
            $table->string('title_en')->nullable();
            $table->string('title_es')->nullable();
            $table->text('description_pt')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_es')->nullable();
            $table->text('keywords')->nullable();
            $table->string('imagem_de_compartilhamento')->nullable();
            $table->string('analytics_ua')->nullable();
            $table->string('analytics_g')->nullable();
            $table->string('codigo_gtm')->nullable();
            $table->string('pixel_facebook')->nullable();
            $table->string('tinify_key')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configuracoes');
    }
}
