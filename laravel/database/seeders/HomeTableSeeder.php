<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'imagem_pt'   => '',
            'imagem_en'   => '',
            'imagem_es'   => '',
            'itens_pt'    => '',
            'itens_en'    => '',
            'itens_es'    => '',
            'motivos1_pt' => 'Grande experiência e alta Capacidade Analítica para oferecer os perfis mais adequados à demanda do cliente',
            'motivos1_en' => '',
            'motivos1_es' => '',
            'motivos2_pt' => 'Metodologia própria garante o preenchimento das vagas ou formação do contrato no mais curto prazo do mercado',
            'motivos2_en' => '',
            'motivos2_es' => '',
            'motivos3_pt' => 'Economia e agilidade durante todo o processo com suporte e consultoria de uma equipe altamente treinada e capacitada',
            'motivos3_en' => '',
            'motivos3_es' => '',
            'frase_selo_pt' => 'A EDC Serviços tem o know how e a experiência da EDC Group - multinacional brasileira com mais de 11 anos de mercado.',
            'frase_selo_en' => '',
            'frase_selo_es' => '',
            'imagem_vagas' => '',
            'frase_pt'    => 'Encontre uma OPORTUNIDADE PROFISSIONAL. Cadastre-se em nosso Banco de Talentos.',
            'frase_en'    => '',
            'frase_es'    => '',
        ]);
    }
}
