<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title_pt'                   => 'EDC SERVIÇOS',
            'title_en'                   => 'EDC SERVICES',
            'title_es'                   => 'SERVICIOS EDC',
            'description_pt'             => '',
            'description_en'             => '',
            'description_es'             => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics_ua'               => '',
            'analytics_g'                => '',
            'codigo_gtm'                 => '',
            'pixel_facebook'             => '',
            'tinify_key'                 => '',
        ]);
    }
}
