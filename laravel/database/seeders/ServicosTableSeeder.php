<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'id'           => 1,
            'ordem'        => 1,
            'capa_pt'      => '',
            'capa_en'      => '',
            'capa_es'      => '',
            'slug_pt'      => 'terceirizacao',
            'slug_en'      => '',
            'slug_es'      => '',
            'titulo_pt'    => 'TERCEIRIZAÇÃO',
            'titulo_en'    => '',
            'titulo_es'    => '',
            'texto_pt'     => '',
            'texto_en'     => '',
            'texto_es'     => '',
            'imagem_pt'    => '',
            'imagem_en'    => '',
            'imagem_es'    => '',
            'vantagens_titulo_pt' => '',
            'vantagens_titulo_en' => '',
            'vantagens_titulo_es' => '',
            'vantagens_texto_pt'  => '',
            'vantagens_texto_en'  => '',
            'vantagens_texto_es'  => '',
        ]);

        DB::table('servicos')->insert([
            'id'           => 2,
            'ordem'        => 2,
            'capa_pt'      => '',
            'capa_en'      => '',
            'capa_es'      => '',
            'slug_pt'      => 'temporarios',
            'slug_en'      => '',
            'slug_es'      => '',
            'titulo_pt'    => 'TEMPORÁRIOS',
            'titulo_en'    => '',
            'titulo_es'    => '',
            'texto_pt'     => '',
            'texto_en'     => '',
            'texto_es'     => '',
            'imagem_pt'    => '',
            'imagem_en'    => '',
            'imagem_es'    => '',
            'vantagens_titulo_pt' => '',
            'vantagens_titulo_en' => '',
            'vantagens_titulo_es' => '',
            'vantagens_texto_pt'  => '',
            'vantagens_texto_en'  => '',
            'vantagens_texto_es'  => '',
        ]);

        DB::table('servicos')->insert([
            'id'           => 3,
            'ordem'        => 3,
            'capa_pt'      => '',
            'capa_en'      => '',
            'capa_es'      => '',
            'slug_pt'      => 'hunting-de-rh',
            'slug_en'      => '',
            'slug_es'      => '',
            'titulo_pt'    => 'HUNTING DE RH',
            'titulo_en'    => '',
            'titulo_es'    => '',
            'texto_pt'     => '',
            'texto_en'     => '',
            'texto_es'     => '',
            'imagem_pt'    => '',
            'imagem_en'    => '',
            'imagem_es'    => '',
            'vantagens_titulo_pt' => '',
            'vantagens_titulo_en' => '',
            'vantagens_titulo_es' => '',
            'vantagens_texto_pt'  => '',
            'vantagens_texto_en'  => '',
            'vantagens_texto_es'  => '',
        ]);
    }
}
